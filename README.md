# Signal chain
```mermaid
graph RL

    bass((Bass Guitar)) --> gc{{Gamechanger<br/>Sustain pedal}}

    subgraph Pedalboard

        subgraph Pre chain processing
            gc --> | Clean signal | hpf{{Broughton HPF}}
            gc --> | Wet signal | tuner{{Boss TU-3s}} 

            hpf --> mt(Darkglass Microtubes<br/>Vintage Deluxe Preamp)

            mt --> hl{{Darkglass<br/>Hyperluminal Compressor}}
        end

        hl ==> |Input| ls2{Boss LS2}
        tuner --> mixer{{Red Panda<br/>3 channel mixer}}
        ls2 ==> |Output| mixer
        ls2 ==> |line A send| p95(MXR Phase 95)

        subgraph Wet effects
            p95 --> env(Fender Pourover<br/>Envelope Filter)
            env --> drone(Rainger FX<br/>Drone Rainger)
        end 

        drone ==> |line A return| ls2
        ls2 ==> |line B send| kot(Analogman<br/>King of Tone)

        subgraph Dry effects
            kot --> ao(Darkglass<br/>Alpha Omicron)
            ao --> fuzz(Earthquaker<br/>Eruptor Fuzz)
            fuzz --> deco(Strymon<br/>Deco)
        end

        deco ==> |line B return| ls2

        subgraph Post chain processing
            mixer --> il(Infinity<br/>Looper)
            il --> ns(Strymon<br/>Nightsky)
            ns --> vp{{Volume pedal}}
            vp --> el(Darkglass Element<br/>Cab Sim)
        end

        subgraph Expression Pedal Control
            ep{{Expression Pedal}} --> me{{Mission<br/>Expressionator}}
            me -.-o deco2(Deco wobble)
            me -.-o delay(Delay control)
            me -.-o loop2(Looper volume)
        end

    end

    ns -.-> |Optional Stereo Pair| axe

    subgraph Amp Stack
        el --> |When Playing Live| amp[(Ampeg MicroCl<br/>50 Watt)]
        amp --> cab[(Ampeg 2x10<br/>Cabinet)]
        el --> |When Recording| axe[\Axe I/O Interface/]
        axe --> max[(computer)]
        axe ---> |When Recording| amp
        el -.-> |Optional monitor| head[(Headphones)]
    end


```